# Minecraft ProxyStopper
Have you ever wanted to restart your BungeeCord proxy while people were online? Annoyed at manually warning them that
the proxy is going to restart soon? Worried they won't see your message? You need ProxyStopper!

ProxyStopper is a free plugin for BungeeCord, Waterfall, and Travertine that adds a /gstop command that has a variety
of options, including a customizable delay before the proxy stops, several warnings in the minutes and seconds leading
up to the proxy stop, and the ability to cancel a stop that's in progress!

WARNING: Unlike ServerStopper, ProxyStopper does not replace the built-in /end command. Please make sure you're taking
advantage of ProxyStopper when stopping your proxy.

## That sounds great. How do I install it?
It's easy, just download the jar file, drag it into plugins, and restart your proxy!
[You can download the release jar from here](https://gitlab.com/AlexMCS/ProxyStopper/tags/)

## How do I use it?
There are a handful of simple commands. Just do <code>/gstop</code> and your proxy will be scheduled to shut down in 1
minute.

Want to shut it down in more than a minute? Just do <code>/gstop \<minutes\></code>.

Accidentally started a stop or want to cancel a stop that someone else started? Easy, just do
<code>/gstop cancel</code>

And you can even stop the proxy immediately like the original stop command with <code>/gstop now</code>

And you can easily look up how to do these things in game with <code>/proxystopper help</code> or
<code>/gstop help</code>

## I don't have a proxy. Can I get this for Bukkit?
Yes! Check out ServerStopper. It's the same concept, but implemented in Bukkit!
[Get ServerStopper here!](https://gitlab.com/AlexMCS/ServerStopper/)

#### How is this different from ServerStopper
Besides using different APIs and commands, it isn't! Much of the codebase is the same.

## Other Information
Here's some more information for the nerdy types. :P

### Permissions
ProxyStopper has only a small handful of permissions.

<code>proxystopper.info</code> - Get information about ProxyStopper with /proxystopper
<br>
<code>proxystopper.help</code> - Get a quick tutorial on how to use ProxyStopper with /proxystopper help
<br>
<code>proxystopper.stop</code> - Stop the proxy with /gstop
<br>
<code>proxystopper.stop.time</code> - Set a custom amount of time with /gstop \<minutes\>
<br>
<code>proxystopper.stop.now</code> - Stop the proxy immediately with /gstop now
<br>
<code>proxystopper.cancel</code> - Cancel a proxy stop with /gstop cancel
<br>
<code>proxystopper.admin</code> - All other permissions ___EXCEPT___ proxystopper.stop.now

### /stop Binding
ProxyStopper does not bind to /stop by default. This is done to maintain compatibility with ServerStopper if installed
within the proxied servers. That said, I had a little problem. When I wanted to stop the proxy from the console, force
of habit would cause me to type "stop" every time, realize I got it wrong, and then retype "gstop". ProxyStopper can
handle this!

If you simply add /stop to your disabled commands in the BungeeCord config.yml, ProxyStopper will bind to /stop!
Because of the way BungeeCord handles disabled commands, that means that when you type "stop" at the proxy console, it
will be passed to ProxyStopper, but when you type "/stop" as an ingame player, it will still be passed to ServerStopper
on the proxied server!

### Bug Reports
Our issue tracker is [here](https://gitlab.com/AlexMCS/ProxyStopper/issues). Please post any bugs you find there and
we'll look into fixing them.

### Development Builds

Want to try out a development build? You can download the latest development build at
[this link](https://gitlab.com/AlexMCS/ProxyStopper/builds/artifacts/master/download?job=build).

Development builds should reflect the very latest code from this repository, but they may also carry strange bugs and
other problems.

Development builds will produce an artifacts.zip file. You can simply open it and find the jar file inside the target
folder.

### For Developers
You are free to make copies of, recompile, and redistribute ProxyStopper however you please. That said, I want to ask
politely that you don't abuse this power and upload re-compiled versions with no changes or worse yet attempt to sell
copies of this plugin. Of course, there's nothing I can do to stop you, but that's just not a good thing to do.

By the way, maybe you wanted to [browse this repository](https://gitlab.com/AlexMCS/ProxyStopper/tree/master)?

### Credits
This plugin was made by ColtonDRG for AlexMCS. :)

### License
Copyright &copy; 2016 AlexMCS (alexmcs.net)
<br>
ProxyStopper is licensed under the MIT License. That means you're free to do whatever you want with it. See
[LICENSE.md](https://gitlab.com/AlexMCS/ProxyStopper/blob/master/LICENSE.md) for more information.