package net.alexmcs.bungee.proxystopper;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;

import java.util.concurrent.TimeUnit;

/**
 * ProxyStopper
 * net.alexmcs.bungee.proxystopper.StopManager
 * ProxyStopper StopManager class.
 * This class is the meat and potatoes of ProxyStopper. It does all the heavy lifting, as well as telling users about
 * the impending shutdown.
 */
class StopManager {

    // Defining some stuff for later use. ;3
    static int clock = -1;
    private static String broadcastPrefix = "" + ChatColor.DARK_GRAY + "[" + ChatColor.DARK_RED + "Alert" + ChatColor.DARK_GRAY + "]" + ChatColor.WHITE + " ";
    private static String preNumberTranslation = "" + ChatColor.DARK_RED + ChatColor.BOLD + "ATTENTION: " + ChatColor.WHITE + "The proxy will be restarting in ";
    private static String numberColor = "" + ChatColor.GOLD + ChatColor.BOLD;
    private static String resetColor = "" + ChatColor.WHITE;
    private static String postNumberTranslation = "" + ChatColor.WHITE + " seconds!";

    // The grand finale. https://youtu.be/j0h2u87JwyA (The method that does all the dirty work)
    private static void stopLoop() {
        // Not really sure why I try wrapped the whole thing. Whatever.
        try {
            // This loop runs as long as the clock is not "stopped", which is an integer equal to -1.
            while (clock != -1) {
                // Do certain things depending on the value of clock
                switch (clock) {
                    // Display notifications at certain intervals.
                    case 3600: // One hour
                    case 1800: // A half an hour
                    case 900: // 15 minutes
                    case 600: // Ten minutes
                    case 300: // Five minutes
                    case 60: // One minute
                    case 30: // Thirty seconds
                    case 15: // 15 seconds
                    // IT'S THE FINAL COUNTDOWN! https://youtu.be/9jK-NcRmVcw (lol memes)
                    case 10: // Ten
                    case 9: // Nine
                    case 8: // Eight
                    case 7: // Seven
                    case 6: // Six
                    case 5: // Five
                    case 4: // Four
                    case 3: // Three
                    case 2: // Two
                    case 1: // One
                        // At all of the above intervals, call doCountdownBroadcast()
                        doCountdownBroadcast(clock);
                        break;
                    // When the clock strikes 0, stop the server.
                    case 0:
                        // Do one last doCountdownBroadcast(), just for old times sake.
                        doCountdownBroadcast(clock);
                        // Get back onto the main thread. Accessing the Bungee API from the async thread (may be?) dangerous.
                        ProxyServer.getInstance().getScheduler().schedule(ProxyServer.getInstance().getPluginManager().getPlugin("ProxyStopper"), new Runnable() {
                            public void run() {
                                // Issue the "end" command from the console.
                                ProxyServer.getInstance().getPluginManager().dispatchCommand(ProxyServer.getInstance().getConsole(), "end");
                            }
                        }, 0, TimeUnit.NANOSECONDS);
                        break;
                }
                // Subtract one from the clock...
                clock = clock - 1;
                // ... and wait one second before looping again.
                Thread.sleep(1000);
            }
        } catch (InterruptedException ex) {
            Thread.currentThread().interrupt();
        }
    }

    // This method is called by the other classes to actually process any operations.
    static void initStop(String heWhoStoppedTheServer, int time) {

        // If the integer passed for time is -1, the user intended to cancel the pending stop.
        if (time == -1) {
            if (clock != -1) {
                // Tell all proxy users that the stop was canceled.
                ProxyServer.getInstance().broadcast(new TextComponent(broadcastPrefix + heWhoStoppedTheServer + resetColor + " has cancelled the proxy restart. The proxy will not restart."));
                // Cancel the stop. Simply setting the clock to -1 is fine, as this will abort the while loop.
                clock = -1;
            }
        }

        // Schedule a proxy stop.
        else {
            // Notify all proxy users that someone has scheduled a stop.
            ProxyServer.getInstance().broadcast(new TextComponent(broadcastPrefix + heWhoStoppedTheServer + resetColor + " has initiated a proxy restart. The proxy will restart in " + numberColor + Integer.toString(time / 60) + resetColor + " minutes."));
            // Set the clock to the integer passed for time. This will reflect seconds in real world time.
            clock = time;
            // We run the while loop asynchronously. This is because we do a lot of Thread.sleep() in order to determine how much real world time has passed. Obviously, doing this on the main thread would hang the server and make it unresponsive.
            ProxyServer.getInstance().getScheduler().runAsync(ProxyServer.getInstance().getPluginManager().getPlugin("ProxyStopper"), new Runnable() {
                @Override
                public void run() {
                    // Wait one second.
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException ex) {
                        Thread.currentThread().interrupt();
                    }
                    // Call the stopLoop() method to get the while loop started.
                    StopManager.stopLoop();
                }
            });
        }
    }

    // This simply warns users about the waning remaining time on the clock when called.
    static private void doCountdownBroadcast(int time) {
        final int remain = time; // Hokey way of dealing with inner-class issue.
        // Run this synchronously. It should be safe to access the chat API asynchronously, but better safe than sorry.
        ProxyServer.getInstance().getScheduler().schedule(ProxyServer.getInstance().getPluginManager().getPlugin("ProxyStopper"), new Runnable() {
            @Override
            public void run() {
                // Do a broadcast. :)
                ProxyServer.getInstance().broadcast(new TextComponent(broadcastPrefix + preNumberTranslation + numberColor + Integer.toString(remain) + postNumberTranslation));
            }
        }, 0, TimeUnit.NANOSECONDS);
    }

}
