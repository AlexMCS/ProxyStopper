package net.alexmcs.bungee.proxystopper;

import net.md_5.bungee.api.plugin.Plugin;

/**
 * ProxyStopper
 * net.alexmcs.bungee.proxystopper.ProxyStopper
 * Main class for ProxyStopper
 *
 * You know, I just read through all the comments and classes. How is this different from ServerStopper??
 * It's not, beyond using different APIs. Maybe a universal version is possible in the future. ;)
 */
public class ProxyStopper extends Plugin {

    // This method is called when the plugin is enabled. (duh!) It registers the commands and does some other cool stuff.
    // There is a little bit of processing done here, but much of it is just spitting out information to the console.
    @Override
    public void onEnable() {

        // Informational
        getLogger().info("ProxyStopper!");
        getLogger().info("version " + this.getDescription().getVersion() + " by ColtonDRG / AlexMCS");

        /**
         * If (AND ONLY IF) the /stop command is listed as a disabled command, ProxyStopper will bind /stop to /gstop.
         * That way, you can type stop at the proxy console to access ProxyStopper features.
         *
         * Since the disabled commands only effect players and not the console, this means that you can easily enable
         * the stop command at the console, and it won't break ServerStopper from the player side of things either way.
         *
         * The following section handles this.
         */
        // Check if /stop is disabled in the BungeeCord configuration.
        if (getProxy().getDisabledCommands().contains("stop")) {
            getLogger().info("I noticed you had the /stop command disabled in the BungeeCord configuration.");
            // Set up the stop command (registration is done in the StopCommand class)
            getProxy().getPluginManager().registerCommand(this, new StopCommand());
            // Informational
            getLogger().info("I took the liberty of binding /stop as an alias to /gstop so you can type stop at the console to stop the proxy.");
            getLogger().info("To learn more about this functionality, see https://gitlab.com/AlexMCS/ProxyStopper/#stop-binding");
        } else {
            // Inform users about this functionality if they are not taking advantage of it.
            getLogger().warning("The /stop command is not disabled in the BungeeCord configuration!");
            getLogger().warning("This plugin supports binding /stop as an alias for /gstop at the console, but you must add /stop to the disabled commands.");
            getLogger().warning("That way, /stop will be passed to the server for ingame players, but act as an alias for /gstop at the console.");
            getLogger().warning("If /stop is not in the disabled commands, ProxyStopper will not bind to /stop.");
            getLogger().warning("If you do not want to take advantage of this functionality, you can safely ignore this warning.");
            getLogger().warning("To learn more about this functionality, see https://gitlab.com/AlexMCS/ProxyStopper/#stop-binding");
        }

        // Check if /end is disabled in the BungeeCord configuration.
        if (getProxy().getDisabledCommands().contains("end")) {
            // Inform users about a common pitfall of ProxyStopper.
            getLogger().info("I noticed you had the /end command disabled in the BungeeCord configuration.");
            getLogger().info("WARNING: Unlike ServerStopper, ProxyStopper does not bind over the default /end command.");
            getLogger().info("WARNING: Even though you disabled /end, it is only disabled for players. Users at the console can still use it.");
            getLogger().info("Please make sure you're taking advantage of ProxyStopper when you stop your proxy. :)");
        } else {
            // Inform users about a common pitfall of ProxyStopper, and how it can be avoided to some extent.
            getLogger().warning("The /end command is not disabled in the BungeeCord configuration!");
            getLogger().warning("WARNING: Unlike ServerStopper, ProxyStopper does not bind over the default /end command.");
            getLogger().warning("To ensure that the proxy doesn't get stopped unexpectedly, I recommend adding /end to the disabled commands!");
            getLogger().warning("Please make sure you're taking advantage of ProxyStopper when you stop your proxy. :)");
            getLogger().warning("You can safely ignore this warning if this was intentional.");
            getLogger().warning("To learn more, see https://gitlab.com/AlexMCS/ProxyStopper/#minecraft-proxystopper");
        }

        // Set up other commands (registration is done in the respective classes)
        getProxy().getPluginManager().registerCommand(this, new GstopCommand());
        getProxy().getPluginManager().registerCommand(this, new ProxystopperCommand());

        // More informational stuff
        getLogger().info("Enabled ProxyStopper");
        getLogger().info("Minecraft ProxyStopper v" + getDescription().getVersion());
        getLogger().info("for BungeeCord/Waterfall");
        getLogger().info("ProxyStopper adds a /gstop command to BungeeCord that warns users before the server shuts down.");
        getLogger().info("https://gitlab.com/AlexMCS/ProxyStopper/");

    }

    // This method is called when the plugin is disabled. (i.e. when the server is stopping) I only use it for informational purposes here.
    public void onDisable() {
        getLogger().info("The proxy is shutting down.");
        getLogger().info("Thank you for using ProxyStopper and ServerStopper! Goodbye.");
    }

}
