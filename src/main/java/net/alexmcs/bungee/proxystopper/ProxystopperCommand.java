package net.alexmcs.bungee.proxystopper;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.plugin.Command;

/**
 * ProxyStopper
 * net.alexmcs.bungee.proxystopper.ProxystopperCommand
 * Command handler for /proxystopper.
 * Gives information about ProxyStopper and help on how to use it. This is a boring class. :P
 */
class ProxystopperCommand extends Command {

    // Register the command
    ProxystopperCommand() {
        super("proxystopper", "", new String[]{"psproxystopper", "psinfo"});
    }

    public void execute(CommandSender sender, String[] args) {

        // Parse the first argument
        String arg1;
        if(args.length == 0) {
            arg1 = "";
        } else {
            arg1 = args[0];
        }

        // Handle the reload command
        if (arg1.equalsIgnoreCase("reload")) {
            /**
             * In a future version of ProxyStopper, there will be translation and configuration files. This feature is
             * not implemented yet, but when it is, there will be a call here to re-initialize the part of the plugin
             * that handles those files. This way, you can change that configuration on the fly without restarting the
             * server.
             */
            sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: This feature is not implemented yet.")));
        }

        // The info command spits out some basic information about ProxyStopper. Nothing too interesting here.
        else if (arg1.equalsIgnoreCase("info") || arg1.equalsIgnoreCase("version") || arg1.equalsIgnoreCase("")) {
            if (sender.hasPermission("proxystopper.info") || sender.hasPermission("proxystopper.admin")) {
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&d&lProxy&4&lStopper&a&l v" + ProxyServer.getInstance().getPluginManager().getPlugin("ProxyStopper").getDescription().getVersion())));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "By &2&lAlex&6&lMCS&r / &b&lColton&4&lDRG&r")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&aTo learn how to use ProxyStopper, do &b&l/proxystopper help&a.")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cMore information: &9http://gitlab.com/AlexMCS/ProxyStopper/")));
            } else {
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: You do not have permission to do that.")));
            }
        }

        // The help command spits out information on how to properly use ProxyStopper. Nothing too interesting here.
        else if (arg1.equalsIgnoreCase("help")) {
            if (sender.hasPermission("proxystopper.help") || sender.hasPermission("proxystopper.admin")) {
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&d&lProxy&4&lStopper&a&l v" + ProxyServer.getInstance().getPluginManager().getPlugin("ProxyStopper").getDescription().getVersion())));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo stop the proxy, use &b&l/gstop")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo stop the proxy immediately, use &b&l/gstop now")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo cancel a pending proxy stop, use &b&l/gstop cancel")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo stop the proxy in five minutes, use &b&l/gstop 5")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo stop the proxy in one hour, use &b&l/gstop 60")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo stop the proxy in any given number of minutes, use &b&l/gstop <minutes>")));
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&cTo learn more about ProxyStopper, use &b&l/proxystopper info")));
            } else {
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: You do not have permission to do that.")));
            }
        }

        // Error for unknown commands.
        else {
            sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: Unknown command.")));
        }

    }

}
