package net.alexmcs.bungee.proxystopper;

import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.plugin.Command;

/**
 * ProxyStopper
 * net.alexmcs.bungee.proxystopper.StopCommand
 * Command handler for /stop.
 * Enabled only if /stop is in the BungeeCord disabled commands. Passes commands straight to /gstop.
 */
class StopCommand extends Command {

    // Register the command
    StopCommand() {
        super("stop");
    }

    @Override
    public void execute(CommandSender commandSender, String[] args) {

        // The following basically parses the String[] args and turns it back into a space-separated argument list so it can be passed back to /gstop later.
        String argpassthrough;
        if (args.length == 0) {
            argpassthrough = "";
        } else {
            StringBuilder argBuilder = new StringBuilder();
            for (String a : args) {
                argBuilder.append(a).append(" ");
            }
            argpassthrough = argBuilder.toString();
        }

        // Pass the command to /gstop
        ProxyServer.getInstance().getPluginManager().dispatchCommand(commandSender, "psgstop " + argpassthrough);

    }

}
