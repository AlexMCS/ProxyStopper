package net.alexmcs.bungee.proxystopper;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.CommandSender;
import net.md_5.bungee.api.ProxyServer;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.plugin.Command;

/**
 * ProxyStopper
 * net.alexmcs.bungee.proxystopper.ProxystopperCommand
 * Command handler for /gstop.
 * This class handles all the main commands that interact with StopManager to stop the proxy.
 */
class GstopCommand extends Command {

    // Register the command
    GstopCommand() {
        super ("gstop", "", new String[]{"psgstop", "psstop", "grestart", "greboot", "proxystop", "stopproxy", "pstop"});
    }

    @Override
    public void execute(CommandSender sender, String[] args) {

        // Determine if the stop request was sent by a player or the console and parse their username
        String senderName;
        if (sender instanceof ProxiedPlayer) {
            ProxiedPlayer player = (ProxiedPlayer) sender;
            senderName = player.getDisplayName();
        } else if (sender == ProxyServer.getInstance().getConsole()) {
            senderName = "Console";
        } else {
            ProxyServer.getInstance().getLogger().warning("An invalid sender has issued the /gstop command.");
            senderName = "??????";
        }

        // Parse the first argument
        String arg1;
        if (args.length == 0) {
            arg1 = "";
        } else {
            arg1 = args[0];
        }

        // The cancel command cancels a pending server stop. This is done by setting the clock in the StopManager class to -1.
        if (arg1.equalsIgnoreCase("cancel") || arg1.equalsIgnoreCase("-1")) {
            if (sender.hasPermission("proxystopper.cancel") || sender.hasPermission("proxystopper.admin")) {
                // Check if the clock is already -1. If so, the server should not be stopping, so there's nothing to cancel.
                if (StopManager.clock == -1) {
                    sender.sendMessage(new TextComponent("" + ChatColor.DARK_RED + ChatColor.BOLD + "ERROR" + ChatColor.RESET + ": The proxy is not stopping, there is nothing to cancel!"));
                } else {
                    // Send a call to the StopManager.initStop() method to cancel the stop.
                    StopManager.initStop(senderName, -1);
                }
            } else {
                sender.sendMessage(new TextComponent("" + ChatColor.DARK_RED + ChatColor.BOLD + "ERROR" + ChatColor.RESET + ": You do not have permission to cancel stopping the proxy."));
            }
        }

        // The help command is a simple alias for /proxystopper help
        else if (arg1.equalsIgnoreCase("help")) {
            ProxyServer.getInstance().getPluginManager().dispatchCommand(sender, "psproxystopper help");
        }

        // For any other command, permission for proxystopper.stop is checked first. I know this seems odd, but it makes for a more efficient workflow.
        else if (sender.hasPermission("proxystopper.stop") || sender.hasPermission("proxystopper.admin")) {

            // The now command will stop the server immediately. This is done by setting the clock in the StopManager class to 0.
            if (arg1.equalsIgnoreCase("now") || arg1.equalsIgnoreCase("0")) {
                if (sender.hasPermission("proxystopper.stop.now")) {
                    // Send a call to the StopManager.initStop() method to stop immediately.
                    StopManager.initStop(senderName, 0);
                } else {
                    sender.sendMessage(new TextComponent("" + ChatColor.DARK_RED + ChatColor.BOLD + "ERROR" + ChatColor.RESET + ": You do not have permission to stop the proxy immediately."));
                }
            }

            // If the first argument is an integer, the server will be scheduled to stop in that many minutes. This is done by multiplying the integer by 60 and setting the clock in the StopManager class to that value.
            else if (isInteger(arg1)) {
                if (sender.hasPermission("proxystopper.stop.time") || sender.hasPermission("proxystopper.admin")) {
                    int stopTime = Integer.valueOf(arg1) * 60;
                    // See doInitStop()
                    doInitStop(sender, senderName, stopTime);
                } else {
                    sender.sendMessage(new TextComponent("" + ChatColor.DARK_RED + ChatColor.BOLD + "ERROR" + ChatColor.RESET + ": You do not have permission to stop the proxy in a custom amount of time."));
                }
            }

            // If no first argument is provided, the server will be scheduled to stop in one minute. This is done by setting the clock in the StopManager class to 60.
            else if (arg1.equalsIgnoreCase("")) {
                // See doInitStop()
                doInitStop(sender, senderName, 60);
            }

            // Error for unknown commands
            else {
                sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: Unknown command.")));
            }

        } else { // this is the else from the proxystopper.stop permission check way up there. :)
            sender.sendMessage(new TextComponent("" + ChatColor.DARK_RED + ChatColor.BOLD + "ERROR" + ChatColor.RESET + ": You do not have permission to stop the proxy."));
        }

    }

    // This method seems a little redundant, but I felt it was the most effective way to handle the error detailed below.
    private static void doInitStop(CommandSender sender, String senderName, int stopTime) {

        // If the server is already stopping (this is determined by checking if the clock in StopManager class is any other value than -1), display an error.
        if (StopManager.clock != -1) {
            sender.sendMessage(new TextComponent(ChatColor.translateAlternateColorCodes('&', "&4&lERROR&r: The proxy is already stopping. If you wish to change the stop perameters, use &b&l/gstop cancel&r to cancel the old stop before starting another one.")));
        } else {
            // Send a call to the StopManager.initStop() method to stop the server in the given number of seconds.
            StopManager.initStop(senderName, stopTime);
        }

    }

    // This basically does some math to determine if a given string can be safely converted to an integer.
    private static boolean isInteger(String s) {
        if(s.isEmpty()) return false;
        for(int i = 0; i < s.length(); i++) {
            if(i == 0 && s.charAt(i) == '-') {
                if(s.length() == 1) return false;
                else continue;
            }
            if(Character.digit(s.charAt(i),10) < 0) return false;
        }
        return true;
    }

}
